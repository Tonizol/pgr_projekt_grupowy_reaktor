import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.svm import SVC
import sklearn.metrics as met
from sklearn.model_selection import train_test_split as tts
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import pymysql
import sqlite3
import sqlalchemy


class Projekt:
    def data_generator(self):
        self.dane = pd.read_csv("Adult_train.tab",sep='\t')
        self.dane_test = pd.read_csv("Adults_test_without_class.tab",sep='\t')
        #engine = create_engine('mysql+pymysql://root:jf8s6avcL@localhost:3306/projekt_grupowy_II')
        #self.dane.to_sql('ad_train', con='mysql+pymysql://root:jf8s6avcL@localhost:3306/projekt_grupowy_II')
        return self

    def dataset_preprocessing(self):

        self.dane['workclass'].replace("?", 'Private', inplace=True)
        self.dane_test['workclass'].replace("?", 'Private', inplace=True)


        # kategoryzacja danych
        self.dane_test["age"] = pd.Categorical(self.dane_test.age).codes
        self.dane_test["workclass"] = pd.Categorical(self.dane_test.workclass).codes
        self.dane_test["fnlwgt"] = pd.Categorical(self.dane_test.fnlwgt).codes
        self.dane_test["education"] = pd.Categorical(self.dane_test.education).codes
        self.dane_test["educatio_num"] = pd.Categorical(self.dane_test.education_num).codes
        self.dane_test["marital_status"] = pd.Categorical(self.dane_test.marital_status).codes
        self.dane_test["occupation"] = pd.Categorical(self.dane_test.occupation).codes
        self.dane_test["relationship"] = pd.Categorical(self.dane_test.relationship).codes
        self.dane_test["race"] = pd.Categorical(self.dane_test.race).codes
        self.dane_test["sex"] = pd.Categorical(self.dane_test.sex).codes
        self.dane_test["capital_gain"] = pd.Categorical(self.dane_test.capital_gain).codes
        self.dane_test["capital_loss"] = pd.Categorical(self.dane_test.capital_loss).codes
        self.dane_test["hours_per_week"] = pd.Categorical(self.dane_test.hours_per_week).codes
        self.dane_test["native_country"] = pd.Categorical(self.dane_test.native_country).codes

        self.dane["y"] = pd.Categorical(self.dane.y).codes
        self.dane["age"] = pd.Categorical(self.dane.age).codes
        self.dane["workclass"] = pd.Categorical(self.dane.workclass).codes
        self.dane["fnlwgt"] = pd.Categorical(self.dane.fnlwgt).codes
        self.dane["education"] = pd.Categorical(self.dane.education).codes
        self.dane["educatio_num"] = pd.Categorical(self.dane.education_num).codes
        self.dane["marital_status"] = pd.Categorical(self.dane.marital_status).codes
        self.dane["occupation"] = pd.Categorical(self.dane.occupation).codes
        self.dane["relationship"] = pd.Categorical(self.dane.relationship).codes
        self.dane["race"] = pd.Categorical(self.dane.race).codes
        self.dane["sex"] = pd.Categorical(self.dane.sex).codes
        self.dane["capital_gain"] = pd.Categorical(self.dane.capital_gain).codes
        self.dane["capital_loss"] = pd.Categorical(self.dane.capital_loss).codes
        self.dane["hours_per_week"] = pd.Categorical(self.dane.hours_per_week).codes
        self.dane["native_country"] = pd.Categorical(self.dane.native_country).codes

        # # korelacja
        # cols_to_corr = ["age", "workclass", "fnlwgt", "education", "educatio_num", "marital_status", "occupation",
        #                 "relationship", "race", "sex", "capital_gain", "capital_loss","hours_per_week","native_country"]
        # corr_m = self.dane[cols_to_corr].corr()
        #
        # # print(corr_m)


    def training_process(self):

        # podział danych na część treningową i część walidacyjną
        kolumny = ["age", "workclass","fnlwgt","education","educatio_num","marital_status","occupation",
                   "relationship","race","sex","capital_gain","capital_loss","hours_per_week","native_country"]
        x_data = self.dane.loc[:, kolumny]
        y_data = self.dane.loc[:, ["y"]]
        self.x_train, self.x_test, self.y_train, self.y_test = tts(x_data, y_data, test_size=0.2)

        self.dane_test = self.dane_test.loc[:,kolumny]


        rf = RandomForestClassifier()

        rf.fit(self.x_train, self.y_train)
        y_pred_train = rf.predict(self.x_train)
        y_pred_test = rf.predict(self.x_test)

        # print(self.dane_test)
        # print("X_TEST")
        # print(self.x_test)
        # print("X_TRAIN")
        # print(self.x_train)
        # print("dane test")
        # print(self.dane_test)

        y_pred = rf.predict(self.dane_test)
        print(y_pred)
        #
        # wynik_train = accuracy_score(self.y_train, y_pred_train)
        # wynik_test = accuracy_score(self.y_test, y_pred_test)
        #
        # print(wynik_train)
        # print(wynik_test)
        # print("Wynik klas")
        # # print(y_pred)







    def testing_process(self):
        pass



    def metrics(self):
        params = {
            'SVC': {
                'kernel': ('linear', 'poly', 'rbf'),
                'C': (0.25, 0.5),
                'gamma': (1, 2)
            }
        }
        # model = SVC(probability=True)
        # model.fit(self.x_train, self.y_train)
        # Y_pred_test = model.predict(self.x_test)
        # met.accuracy_score(self.Y_test, Y_pred_test)
        # print(met.classification_report(self.Y_test, Y_pred_test))
        # met.confusion_matrix(self.Y_test, Y_pred_test)

    def plotting(self):
        pass



pro = Projekt()
pro.data_generator()
pro.dataset_preprocessing()
pro.training_process()

pro.metrics()







#dane.replace("?", np.NaN, inplace=True)
#print(dane)
#print(corr_m)
#dane.head(100)
#dane["age"].hist()
#print(dane.groupby(["workclass"])["education"].agg(["min", "max", "mean", "median", "count"]))
#dane.isnull().sum()



